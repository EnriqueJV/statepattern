interface MobileAlertState {
    fun alert(ctx: AlertStateContext)
}

class AlertStateContext {
    private var currentState: MobileAlertState? = null

    init {
        currentState = Vibration()
    }

    fun setState(state: MobileAlertState) {
        currentState = state
    }

    fun alert() {
        currentState!!.alert(this)
    }
}

class Vibration : MobileAlertState {

    override fun alert(ctx: AlertStateContext) {
        println("Vibration...")
    }
}

class Silent : MobileAlertState {

    override fun alert(ctx: AlertStateContext) {
        println("Silent...")
    }
}

class Sound : MobileAlertState {

    override fun alert(ctx: AlertStateContext) {
        println("Ring Ring Ring")
    }
}

fun main(args: Array<String>) {
    val stateContext = AlertStateContext()
    stateContext.alert()
    stateContext.alert()
    stateContext.setState(Silent())
    stateContext.alert()
    stateContext.alert()
    stateContext.setState(Sound())
    stateContext.alert()
}